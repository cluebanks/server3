from fastapi.testclient import TestClient
from fastapi import Depends

from ..main import app
from ..database import SessionLocal, engine, get_db

from .. import models

client = TestClient(app)

db=SessionLocal()
db.query(models.User).delete()
db.commit()

def test_get_accounts():
    pass

def test_get_account():
    pass

def test_post_account():
    pass
  
def test_credit_account():
    pass

def test_debit_account():
    pass

def test_transfer_account():
    pass

def test_delete_account():
    pass

