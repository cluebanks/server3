from fastapi.testclient import TestClient
from fastapi import Depends

from ..main import app
from ..database import SessionLocal, engine, get_db

from .. import models

client = TestClient(app)

db=SessionLocal()
db.query(models.User).delete()
db.commit()

def test_get_users():
    user1=client.post("/users", json={"firstname":"François-Xavier","lastname":"De Nys"})
    user2=client.post("/users", json={"firstname":"Jean","lastname":"Dupont"})
    user3=client.post("/users", json={"firstname":"Albert","lastname":"Durant"})
    user3=client.post("/users", json={"firstname":"Joseph","lastname":"Duchmol"})
    user3=client.post("/users", json={"firstname":"Olivier","lastname":"Truc"})

    response = client.get("/users")   
    assert response.status_code == 200   
    users=response.json()
    assert users
    assert len(users)==5
    assert users[0]['firstname']=="François-Xavier"
    assert users[4]['lastname']=="Truc"

    response = client.get("/users?offset=2")   
    assert response.status_code == 200   
    users=response.json()
    assert users
    assert len(users)==3
    assert users[0]['firstname']=="Albert"
    assert users[2]['lastname']=="Truc"
    
    response = client.get("/users?limit=2")   
    assert response.status_code == 200   
    users=response.json()
    assert users
    assert len(users)==2
    assert users[0]['firstname']=="François-Xavier"
    assert users[1]['lastname']=="Dupont"

    response = client.get("/users?offset=1&limit=1")   
    assert response.status_code == 200   
    users=response.json()
    assert users
    assert len(users)==1
    assert users[0]['firstname']=="Jean"

    db.query(models.User).delete()
    db.commit()

def test_get_user():
    user1=client.post("/users", json={"firstname":"François-Xavier","lastname":"De Nys"}).json()

    response = client.get("/users/"+str(user1['id']))   
    assert response.status_code == 200   
    user=response.json()
    assert user
    assert user['firstname']=="François-Xavier"
    assert user['lastname']=="De Nys"
    db.query(models.User).delete()
    db.commit()

def test_post_user():
    response=client.post("/users", json={"firstname":"François-Xavier"})
    assert response.status_code == 422   
    response=client.post("/users", json={"lastname":"De Nys"})
    assert response.status_code == 422   

    response=client.post("/users", json={"firstname":"François-Xavier","lastname":"De Nys"})
    assert response.status_code == 200   
    user_id=response.json()['id']
    user = client.get("/users/"+str(user_id)).json()
    assert user == {"id": user_id, "firstname": "François-Xavier", "lastname": "De Nys", "address": None, "birthdate": None, "lat": None, "lng": None, "accounts": []}   

    response=client.post("/users", json={"firstname":"François-Xavier","lastname":"De Nys", "birthdate":"1974-12-26", "address": "Kamerdelle 30"})
    assert response.status_code == 200   
    user_id=response.json()['id']
    user = client.get("/users/"+str(user_id)).json()
    assert user == {"id": user_id, "firstname": "François-Xavier", "lastname": "De Nys", "address": "Kamerdellelaan 30, 1180 Ukkel, Belgium", "birthdate": "1974-12-26", "lat": 50.8017, "lng": 4.34804, "accounts": []}   
    db.query(models.User).delete()
    db.commit()
  
def test_post_user():
    pass

def test_delete_user():
    pass

