from .routers import users, accounts
from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import JSONResponse

from . import models
from .database import SessionLocal, engine

import inspect
import sqlalchemy

models.Base.metadata.create_all(bind=engine)

app = FastAPI(title="ClueBanks",
    description="This is a very fancy project, with auto docs for the API and everything",
    version="1.0",)

app.mount("/static", StaticFiles(directory="./app/static"))

@app.exception_handler(sqlalchemy.exc.SQLAlchemyError)
async def unicorn_exception_handler(request: Request, exc: Exception):
    return JSONResponse(
        status_code=500,
        content={"detail": exc.__class__.__name__ },
    )

app.include_router(users.router, prefix="/users", tags=["users"])
app.include_router(accounts.router, prefix="/accounts", tags=["accounts"])
