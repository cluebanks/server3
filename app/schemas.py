from typing import List, Optional
from pydantic import BaseModel, Field
import datetime
from . import models


class AccountCreate(BaseModel):
    account_type: models.AccountType
    user_id: int

class UserCreate(BaseModel):
    firstname   : str
    lastname    : str
    address     : Optional[str]
    birthdate   : Optional[datetime.date]

class UserUpdate(BaseModel):
    firstname   : Optional[str]
    lastname    : Optional[str]
    address     : Optional[str]
    birthdate   : Optional[datetime.date]

class AccountShortResponse(BaseModel):
    id: int
    balance     : int
    number      : str
    class Config:
        orm_mode = True

class UserShortResponse(BaseModel):
    id          : int
    firstname   : str
    lastname    : str
    class Config:
        orm_mode = True

class AccountResponse(BaseModel):
    id: int
    balance     : int
    number      : str
    account_type: models.AccountType
    user: UserShortResponse
    class Config:
        orm_mode = True

class UserResponse(BaseModel):
    id          : int
    firstname   : Optional[str]
    lastname    : Optional[str]
    address     : Optional[str]
    birthdate   : Optional[datetime.date]
    lat         : Optional[float]
    lng         : Optional[float]
    accounts    : List[AccountShortResponse]
    class Config:
        orm_mode = True

class AccountTransferResponse(BaseModel):
    debited_account: AccountResponse
    credited_account: AccountResponse


class Amount(BaseModel):
    class Config:
        orm_mode = True

    amount: int

class DeletedRows(BaseModel):
    deleted_rows: int