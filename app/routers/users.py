from typing import List
from fastapi import APIRouter, Depends, HTTPException
from urllib.parse import urlencode
from sqlalchemy.orm import Session
from .. import models, schemas
from app.database import get_db
from typing import Optional
import httpx
import json
from app import config

router = APIRouter()

async def geocode(address:str):
    response = await httpx.AsyncClient().get('https://maps.googleapis.com/maps/api/geocode/json?%s' %urlencode({"address":address, "key": config.settings.google_app_key}))
    if (response.status_code!=200):
        raise Exception("Google geocoding API did not respond correctly")

    response = json.loads(response.text)
    if (response['status']=='OK'):
        return {
            "lat": response['results'][0]['geometry']['location']['lat'], 
            "lng":response['results'][0]['geometry']['location']['lng'], 
            "address":response['results'][0]['formatted_address']
        }
    else:
        raise Exception("Invalid address")

@router.get("", response_model=List[schemas.UserResponse])
def get_all(offset: Optional[int] = None, limit: Optional[int] = None, db: Session = Depends(get_db)):
    return db.query(models.User).offset(offset).limit(limit).all()

@router.get("/{user_id}", response_model=schemas.UserResponse)
def get_one(user_id: int, db: Session = Depends(get_db)):
    db_user=db.query(models.User).filter(models.User.id == user_id).first()
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

@router.post("", response_model=schemas.UserResponse)
async def create(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = models.User(**user.__dict__)
    if user.address:
        db_user.update_from_dict(** await geocode(user.address))
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

@router.put("/{user_id}", response_model=schemas.UserResponse)
async def update(user_id: int, user: schemas.UserUpdate, db: Session = Depends(get_db)):
    db_user=db.query(models.User).filter(models.User.id == user_id).with_for_update().first()
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    db_user.update_from_schema(user)
    db_user.update_from_dict(** await geocode(user.address))
    db.commit()    
    return db_user

@router.delete("/{user_id}", response_model=schemas.DeletedRows)    
def delete(user_id: int, db: Session = Depends(get_db)):
    query=db.query(models.User).filter(models.User.id==user_id).with_for_update()
    db_user=query.first()
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    for account in db_user.accounts:
        if account.balance>0:
            raise HTTPException(status_code=400, detail="User accounts are not empty") 

    deleted_rows=query.delete()
    db.commit()
    return {"deleted_rows": deleted_rows}
