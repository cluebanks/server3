from typing import List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from .. import models, schemas
from app.database import get_db
from typing import Optional
import uuid
import time

router = APIRouter()
@router.get("/test")
async def get_test(db: Session = Depends(get_db)):
    time.sleep(5)
    return {'hello': 'you'}

@router.get("", response_model=List[schemas.AccountResponse])
def get_all(offset: Optional[int] = None, limit: Optional[int] = None, db: Session = Depends(get_db)):
    return db.query(models.Account).offset(offset).limit(limit).all()

@router.get("/{account_number}", response_model=schemas.AccountResponse)
def get_one(account_number: str, db: Session = Depends(get_db)):
    db_account=db.query(models.Account).filter(models.Account.number == account_number).first()
    if db_account is None:
        raise HTTPException(status_code=404, detail="Account not found")
    return db_account

@router.post("", response_model=schemas.AccountResponse)
def create(account: schemas.AccountCreate, db: Session = Depends(get_db)):
    db_account = models.Account(**account.__dict__)
    db_account.balance=0
    db_account.number=str(uuid.uuid4())
    db.add(db_account)
    db.commit()
    db.refresh(db_account)
    return db_account

@router.post("/{account_number}/credit", response_model=schemas.AccountResponse)
def credit(account_number: str, credit: schemas.Amount, db: Session = Depends(get_db)):
    if (credit.amount<=0):
        raise HTTPException(status_code=400, detail="Amount must be stricly positive")

    db_account = db.query(models.Account).filter(models.Account.number==account_number).with_for_update().first()
    if db_account is None:
        raise HTTPException(status_code=404, detail="Account not found")

    db_account.balance+=credit.amount
    db.add(db_account)
    db.commit()
    db.refresh(db_account)
    return db_account

@router.post("/{account_number}/debit", response_model=schemas.AccountResponse)
def debit(account_number: str, debit: schemas.Amount, db: Session = Depends(get_db)):
    if (debit.amount<=0):
        raise HTTPException(status_code=400, detail="Amount must be stricly positive")

    db_account = db.query(models.Account).filter(models.Account.number==account_number).with_for_update().first()
    if db_account is None:
        raise HTTPException(status_code=404, detail="Account not found")
    if (db_account.balance<debit.amount):
        raise HTTPException(status_code=400, detail="Not enough credit")

    db_account.balance-=debit.amount
    db.add(db_account)
    db.commit()
    db.refresh(db_account)
    return db_account

@router.post("/{debited_account_number}/transfer-to/{credited_account_number}", response_model=schemas.AccountTransferResponse)
def transfer(debited_account_number: str, credited_account_number: str, transfer: schemas.Amount, db: Session = Depends(get_db)):
    if (transfer.amount<=0):
        raise HTTPException(status_code=400, detail="Amount must be stricly positive")

    db_debited_account = db.query(models.Account).filter(models.Account.number==debited_account_number).with_for_update().first()
    if db_debited_account is None:
        raise HTTPException(status_code=404, detail="Origin account not found")
    if (db_debited_account.balance<transfer.amount):
        raise HTTPException(status_code=400, detail="Not enough credit into origin account")

    db_credited_account = db.query(models.Account).filter(models.Account.number==credited_account_number).with_for_update().first()
    if db_credited_account is None:
        raise HTTPException(status_code=404, detail="Destination account not found")

    db_debited_account.balance-=transfer.amount
    db_credited_account.balance+=transfer.amount
    db.add(db_debited_account)
    db.add(db_credited_account)
    db.commit()
    db.refresh(db_debited_account)
    db.refresh(db_credited_account)
    return {"debited_account":db_debited_account, "credited_account": db_credited_account}

@router.delete("/{account_number}", response_model=schemas.DeletedRows)    
def delete(account_number: str, db: Session = Depends(get_db)):
    query=db.query(models.Account).filter(models.Account.number==account_number).with_for_update()
    db_account=query.first()
    if db_account is None:
        raise HTTPException(status_code=404, detail="Account not found")

    if db_account.balance>0:
        raise HTTPException(status_code=400, detail="Account is not empty")

    deleted_rows=query.delete()
    db.commit()
    return {"deleted_rows": deleted_rows}
