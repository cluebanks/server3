from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float, Date, Enum
from sqlalchemy.orm import relationship
import datetime
import enum
from typing import List, Optional
from .database import Base




class AccountType(enum.Enum):
    silver = "silver"
    gold = "gold"
    platinium = "platinium"

class Updatable():
    def update_from_schema(self, obj):
        for key, value in obj.__dict__.items():
            if hasattr(self, key) and (obj.__fields__[key].required or value is not None):
                setattr(self, key, value)

    def update_from_dict(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)

class Account(Base, Updatable):
    __tablename__ = 'account'
    __table_args__ = {'mysql_engine':'InnoDB'}

    serialize_only = ('id', 'balance', 'user', 'number', 'account_type', 'user.firstname', 'user.lastname')
    serialize_rules = ('-user.accounts',)

    id          : int = Column(Integer, primary_key=True)
    balance     : int = Column(Integer, nullable=False)
    number      : str = Column(String(36), nullable=False, index=True, unique=True)
    account_type: AccountType = Column('account_type', Enum(AccountType))
    user_id     : int = Column(Integer, ForeignKey('user.id', ondelete="cascade"))

    user        = relationship("User", back_populates="accounts", lazy='joined')

class User(Base, Updatable):
    __tablename__ = 'user'
    __table_args__ = {'mysql_engine':'InnoDB'}

    serialize_only = ('id', 'firstname', 'lastname', 'address', 'lat', 'lng', 'birthdate', 'accounts.id', 'accounts.number', 'accounts.balance')
    serialize_rules = ('-accounts.user',)

    id          : int = Column(Integer, primary_key=True)
    firstname   : str = Column(String(128), nullable=False)
    lastname    : str = Column(String(128), nullable=False)
    address     : str = Column(String(256), nullable=True)
    lat         : float = Column(Float(), nullable=True)
    lng         : float = Column(Float(), nullable=True)
    birthdate   : datetime.date = Column(Date(), nullable=True)

    accounts    : Optional[List[Account]] = relationship("Account", back_populates="user", lazy='joined')

